<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Apple
 *
 * @property string $item_id
 * @property string $display_name
 * @property int    $price
 * @property string $currency
 *
 * @package App\Models
 */
class Apple extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'item_id',
        'display_name',
        'price',
        'currency',
    ];
}
