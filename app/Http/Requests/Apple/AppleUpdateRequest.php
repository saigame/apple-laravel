<?php

namespace App\Http\Requests\Apple;

use Illuminate\Foundation\Http\FormRequest;

/**
 *
 * @property-read string $item_id
 * @property-read string $display_name
 * @property-read int    $price
 * @property-read string $currency
 *
 */
class AppleUpdateRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_id'      => ['string'],
            'display_name' => ['string'],
            'price'        => ['integer'],
            'currency'     => ['string'],
        ];
    }
}
