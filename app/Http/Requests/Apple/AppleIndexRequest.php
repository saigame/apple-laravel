<?php

namespace App\Http\Requests\Apple;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property-read integer $page
 *
 */
class AppleIndexRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

        ];
    }
}
