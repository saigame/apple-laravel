<?php

namespace App\Http\Requests\Apple;

use Illuminate\Foundation\Http\FormRequest;

/**
 *
 * @property-read string $item_id
 * @property-read string $display_name
 * @property-read int    $price
 * @property-read string $currency
 *
 */
class AppleCreateRequest extends FormRequest
{

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'item_id'      => ['required', 'string'],
            'display_name' => ['required', 'string'],
            'price'        => ['required', 'integer'],
            'currency'     => ['required', 'string'],
        ];
    }
}
