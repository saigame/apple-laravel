<?php

namespace App\Http\Controllers;

use App\Http\Requests\Apple\AppleCreateRequest;
use App\Http\Requests\Apple\AppleIndexRequest;
use App\Http\Requests\Apple\AppleUpdateRequest;
use App\Models\Apple;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\App;

class AppleCtrl extends Controller
{

    public function create(AppleCreateRequest $request)
    {
        $apple = new Apple();
        $apple->item_id = $request->item_id;
        $apple->display_name = $request->display_name;
        $apple->price = $request->price;
        $apple->currency = $request->currency;
        $apple->save();

        return response($apple->toArray());
    }

    public function update(AppleUpdateRequest $request, Apple $apple)
    {
        $request->item_id ? $apple->item_id = $request->item_id : null;
        $request->display_name ? $apple->display_name = $request->display_name : null;
        $request->price ? $apple->price = $request->price : null;
        $request->currency ? $apple->currency = $request->currency : null;
        $apple->save();

        return response($apple->toArray());
    }

    public function view(Apple $apple)
    {
        return response($apple->toArray());
    }

    public  function index(AppleIndexRequest $request){
        $users = Apple::paginate(5);

        return response($users->toArray());
    }
}
