<?php

use App\Http\Controllers\AppleCtrl;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});

Route::post('apples', [AppleCtrl::class, 'create']);
Route::put('apples/{apple}', [AppleCtrl::class, 'update']);
Route::get('apples/{apple}', [AppleCtrl::class, 'view']);
Route::get('apples', [AppleCtrl::class, 'index']);
